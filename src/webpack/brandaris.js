// Webpack should also output CSS based on the following files
import "./../scss/brandaris.scss";

// JavaScripts
import "./../js/_variables.js";
import "./../js/functions.js";
import "./../js/notifications.js";
import "./../js/slider.js";
import "./../js/tabs.js";
import "./../js/toggle.js";
