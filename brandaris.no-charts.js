// For module import
import "./src/js/_variables.js";
import "./src/js/functions.js";
import "./src/js/notifications.js";
import "./src/js/slider.js";
import "./src/js/tabs.js";
import "./src/js/toggle.js";
