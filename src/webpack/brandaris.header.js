// JavaScripts
var outdatedBrowserRework = require("outdated-browser-rework");
outdatedBrowserRework({
  browserSupport: {
    "Chrome": 57, // Includes Chrome for mobile devices
    "Edge": 39,
    "Safari": 10,
    "Mobile Safari": 10,
    "Firefox": 50,
    // You could specify a version here if you still support IE in 2017.
    // You could also instead seriously consider what you're doing with your time and budget
    "IE": false
  },
  requireChromeOnAndroid: false
});

document.createElement("picture");
var picturefill = require("picturefill");
picturefill();
