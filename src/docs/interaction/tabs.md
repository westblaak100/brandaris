---
layout: docs
title: Tabs
group: interaction
---

Tab. Tab-tab-tab. Tab.

### Default

The tabs are flexible.

{% example html %}
<div class="row">
  <div class="col">
    <ul class="tabs-nav" data-tabs-nav="tabs-example">
      <li class="active" data-tab-go="0">Tab 1</li>
      <li data-tab-go="1">Tab 2</li>
      <li data-tab-go="2">Tab 3</li>
    </ul>
  </div>
</div>
<div class="row mb-2">
  <div class="col">
    <div id="tabs-example" class="tabs">
      <div class="tabs-content active bg-primary" data-tab="0">
        Tab 1 content
      </div>
      <div class="tabs-content bg-secondary" data-tab="1">
        Tab 2 content
      </div>
      <div class="tabs-content bg-tertiary" data-tab="2">
        Tab 3 content
      </div>
    </div>
  </div>
</div>
{% endexample %}

### Alternative

{% example html %}
<div class="row">
  <div class="col">
    <ul class="tabs-nav tabs-nav-alt" data-tabs-nav="tabs-example2">
      <li class="active" data-tab-go="0">Tab 1</li>
      <li data-tab-go="1">Tab 2</li>
      <li data-tab-go="2">Tab 3</li>
    </ul>
  </div>
</div>
<div class="row mb-2">
  <div class="col">
    <div id="tabs-example2" class="tabs">
      <div class="tabs-content active bg-white" data-tab="0">
        Tab 1 content
      </div>
      <div class="tabs-content bg-secondary" data-tab="1">
        Tab 2 content
      </div>
      <div class="tabs-content bg-tertiary" data-tab="2">
        Tab 3 content
      </div>
    </div>
  </div>
</div>
{% endexample %}
