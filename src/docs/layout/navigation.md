---
layout: docs
title: Navigation
group: layout
---

Need some food? See this AWESOM-O menu!

### Navigation menu

This menu is responsive: full screen on small devices.

{% example html %}
<nav class="row middle nav">
  <input id="nav-toggle-example" class="nav-toggle" type="checkbox">
  <label for="nav-toggle-example" class="nav-label pad-none">Menu</label>
  <div class="col-12 nav-menu-wrap align-center">
    <div class="nav-logo">
      <a href="{{ site.baseurl }}/" title="">Brand logo</a>
    </div>
    <ul class="nav-menu">
      <li class="active"><a href="{{ site.baseurl }}/" title="">Item 1</a></li>
      <li><a href="#" title="">Item 2</a></li>
      <li><a href="#" title="">Item 3</a></li>
      <li><a href="#" title="">Item 4</a></li>
      <li class="nav-right"><a href="#" title=""><span class="icon">{% icon user.svg %}</span> Login</a></li>
    </ul>
  </div>
</nav>
{% endexample %}
