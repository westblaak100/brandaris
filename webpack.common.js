const path = require('path');

const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = {
  entry: {
    'brandaris': './src/webpack/brandaris.js',
    'brandaris.charts': './src/webpack/brandaris.charts.js',
    'brandaris.docs': './src/webpack/brandaris.docs.js',
    'brandaris.header': './src/webpack/brandaris.header.js',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/assets/scripts')
  },
  resolve: {
    alias: {
      JS: path.resolve(__dirname, 'src/js/'),
    },
  },
  module: {
    rules: [
      {
        enforce: "pre",
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: "eslint-loader",
      },
      {
        // JavaScript files
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['babel-preset-env']
          }
        }
      },
      {
        // images :)
        test: /\.(jpg|png|svg)$/,
        include: [
          path.resolve(__dirname, "assets/images")
        ],
        use: [{
          loader: "url-loader",
          options: {
            limit: 30000,
            name: "./../images/[name].[ext]",
          }
        }]
      },
      {
        // fonts
        test: /\.(eot|woff|woff2|ttf|svg)$/,
        include: [
          path.resolve(__dirname, "assets/fonts")
        ],
        use: [{
          loader: "url-loader",
          options: {
            limit: 30000,
            name: "./../fonts/[name].[ext]",
          }
        }]
      }
    ]
  },
  plugins: [
    new StyleLintPlugin({
      context: 'src/scss/',
      emitErrors: true,
      failOnError: false,
      syntax: "scss",
      quiet: false
    })
  ]
};
