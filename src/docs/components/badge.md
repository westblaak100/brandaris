---
layout: docs
title: Badges and circles
group: components
---

Wear your badge with pride.

### Default badge

{% example html %}
<p>This is some text <span class="badge bg-primary clr-white">with a nice badge</span> in it.</p>
{% endexample %}

### Default circle

{% example html %}
<p>This is some text <span class="badge badge-circle bg-secondary clr-white">1</span> in it.</p>
{% endexample %}
