---
layout: docs
title: Images
group: base
---

An image says almost more than thousand lines of code.

### Background

Want to use an image as a background for a div? Here's the way to do it. By default the content on top of the image is vertically aligned in the middle. For top and bottom alignment you can add the classes `.top` and `.bottom` to the `.row`-element.

{% example html %}
<div class="bg-image mb-2">
  <div>
    <img src="{{ site.baseurl }}/assets/images/repository.jpg" class="img-responsive">
  </div>
  <div class="row center">
    <div class="col-6">
      <div class="box bg-black clr-white">
        <p>This is an awesome box on top of a image!</p>
        <p>We can do whatever we want here.</p>
        <a href="#" title="Button title" class="btn btn-block">Button text</a>
      </div>
    </div>
  </div>
</div>
{% endexample %}

### Overlay

Change the opacity of the image by adding `.img-overlay-dark` or `.img-overlay-light` in combination with `.img-overlay`.

#### Dark

{% example html %}
<div class="img-overlay img-overlay-dark mb-2">
  <img src="{{ site.baseurl }}/assets/images/repository.jpg" class="img-responsive">
</div>
{% endexample %}

#### Light

{% example html %}
<div class="img-overlay img-overlay-light mb-2">
  <img src="{{ site.baseurl }}/assets/images/repository.jpg" class="img-responsive">
</div>
{% endexample %}

### Responsive

Add `.img-responsive` to give the image a `max-width` of 100%.

### Flip

Flippin' images with `.img-flip`.
<img src="{{ site.baseurl }}/assets/images/repository.jpg" class="img-flip img-responsive">

{% highlight html %}
<img src="{{ site.baseurl }}/assets/images/repository.jpg" class="img-flip">
{% endhighlight %}
