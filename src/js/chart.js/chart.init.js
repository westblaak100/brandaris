import Chart from "chart.js";
import "moment";
import { forEach } from "JS/functions";

// Set defaults
Chart.defaults.global.defaultFontColor = "#000";
Chart.defaults.global.defaultColor = "#a1a1a1";
Chart.defaults.global.elements.responsive = true;
Chart.defaults.global.legend.position = "bottom";
Chart.defaults.global.maintainAspectRatio = false;
var renderedCharts = {};

document.addEventListener("DOMContentLoaded", function() {
  renderCharts();
});

export function renderCharts() {
  var charts = document.querySelectorAll(".chart-container");
  forEach(charts, function (chart) {
    loadJSON(chart.dataset.source, function (response) {
      // Parse JSON string into object
      var json = JSON.parse(response);

      // Create chart
      renderedCharts[chart.firstElementChild.id] = new Chart(chart.firstElementChild.id, {
        type: json.type,
        data: insertChartColor(json.data),
        options: json.options
      });
    });
  });
}

export function loadJSON(jsonFile, callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", jsonFile, true);
  xobj.onreadystatechange = function () {
    if (xobj.readyState === 4 && xobj.status === 200) {
      // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

export function insertChartColor(object) {
  for (var property in object) {
    // check if object is array
    var isArray;
    var color;
    if (Object.prototype.toString.call(object[property]) === "[object Array]") {
      isArray = true;
    } else {
      isArray = false;
    }

    // check if property is color
    if (property === "color") {
      if (isArray) {
        object["backgroundColor"] = new Array();
        object["borderColor"] = new Array();
        object["hoverBackgroundColor"] = new Array();
        object["hoverBorderColor"] = new Array();
        for (var i = 0; i < object[property].length; i++) {
          color = getCssColor(object[property][i]);
          object["backgroundColor"].push("rgba(" + color + ",.75)");
          object["borderColor"].push("rgba(" + color + ",1)");
          object["hoverBackgroundColor"].push("rgba(" + color + ",.5)");
          object["hoverBorderColor"].push("rgba(" + color + ",.75)");
        }
      } else {
        color = getCssColor(object[property]);
        // set color
        object["backgroundColor"] = "rgba(" + color + ",.75)";
        object["borderColor"] = "rgba(" + color + ",1)";
        object["hoverBackgroundColor"] = "rgba(" + color + ",.5)";
        object["hoverBorderColor"] = "rgba(" + color + ",.75)";
      }

      // delete non used color property
      delete object[property];
    } else if (typeof object[property] === "object") {
      // recursive to the rescue
      object[property] = insertChartColor(object[property]);
    }
  }
  return object;
}

export function getCssColor(name) {
  // create a node and add needed style
  var clrNode = document.createElement("div");
  clrNode.className = "clr-" + name;
  document.getElementsByTagName("body")[0].appendChild(clrNode);

  // grab color from style
  var color = window.getComputedStyle(clrNode, null).getPropertyValue("color");
  color = color.replace("rgb(", "").replace(")", "");

  // remove created node
  clrNode.remove();

  return color;
}
