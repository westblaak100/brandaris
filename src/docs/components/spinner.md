---
layout: docs
title: Spinner
group: components
---

### Default

By default you can simply add a spinner by using the following snippet.

{% example html %}
<div class="mb-2">
  <span class="spinner"></span>
</div>
{% endexample %}

### Colors

Play with all the spinners colors:
{% example html %}
<div class="mb-2">
  <span class="spinner spinner-primary"></span>
</div>
{% endexample %}

{% example html %}
<div class="mb-2">
  <span class="spinner spinner-secondary"></span>
</div>
{% endexample %}

{% example html %}
<div class="mb-2">
  <span class="spinner spinner-tertiary"></span>
</div>
{% endexample %}


### Size

Increase size of spinner by adding `h1` - `h6` class

{% example html %}
<div class="mb-2">
  <span class="spinner spinner-primary h1"></span>
</div>
{% endexample %}

{% example html %}
<div class="mb-2">
  <span class="spinner h3"></span>
</div>
{% endexample %}
