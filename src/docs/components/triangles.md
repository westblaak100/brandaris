---
layout: docs
title: Triangles
group: components
---

Need to grab the viewers attention? Use these triangles to point them in the right direction. Triangles can be added to any element with a colored background class.

### Bottom

<div class="row center mb-2">
  <div class="col-6 mb-2">
    <div class="box bg-primary clr-white align-center triangle-bottom mb-2">
      <span>This is a box with a triangle at the bottom.</span>
    </div>
  </div>
</div>

{% highlight html %}
<div class="row center">
  <div class="col-6">
    <div class="box bg-primary clr-white align-center triangle-bottom">
      <span>This is a box with a triangle at the bottom.</span>
    </div>
  </div>
</div>
{% endhighlight %}

### Left

{% example html %}
<div class="row center">
  <div class="col-6">
    <div class="box bg-primary clr-white align-center triangle-left mb-2">
      <span>This is a box with a triangle at the left.</span>
    </div>
  </div>
</div>
{% endexample %}

### Right

{% example html %}
<div class="row center">
  <div class="col-6">
    <div class="box bg-primary clr-white align-center triangle-right mb-2">
      <span>This is a box with a triangle at the right.</span>
    </div>
  </div>
</div>
{% endexample %}

### Top

<div class="row center mt-2">
  <div class="col-6 mt-2">
    <div class="box bg-primary clr-white align-center triangle-top mb-2">
      <span>This is a box with a triangle at the top.</span>
    </div>
  </div>
</div>

{% highlight html %}
<div class="row center">
  <div class="col-6">
    <div class="box bg-primary clr-white align-center triangle-top">
      <span>This is a box with a triangle at the top.</span>
    </div>
  </div>
</div>
{% endhighlight %}

## Responsiveness

All triangles can be used from the smallest screens and up (`.triangle-{direction}`), but also starting from specific breakpoints (`.triangle-{direction}-{size}`). For example the combination of `.triangle-bottom` and `.triangle-left-md` will give a triangle at the bottom and from screen size `md` and up it will be moved to the left.
