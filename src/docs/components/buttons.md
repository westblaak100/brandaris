---
layout: docs
title: Buttons
group: components
---

Buttons, buttons, buttons: everybody gets buttons!

### Default

{% example html %}
<p>Come and see: <button class="btn">The default button</button></p>
{% endexample %}

### Colors

Play with all the button colors:

{% example html %}
<p><button class="btn">The default button</button></p>
<p><a href="#" class="btn btn-primary">The primary button</a></p>
<p><button class="btn btn-secondary">The secondary button</button></p>
<p><button class="btn btn-tertiary">The tertiary button</button></p>
<p><button class="btn btn-white">The white button</button></p>
<p><button class="btn btn-black">The black button</button></p>
<p><button class="btn btn-gray-light">The gray-light button</button></p>
<p><button class="btn btn-gray">The gray button</button></p>
<p><button class="btn btn-gray-dark">The gray-dark button</button></p>
{% endexample %}

### Sizes

From `.btn-sm` to `.btn-xl`, one will fit.

{% example html %}
<p><button class="btn btn-black btn-sm">The small button</button></p>
<p><button class="btn btn-black">The default button</button></p>
<p><button class="btn btn-black btn-lg">The large button</button></p>
<p><button class="btn btn-black btn-xl">The extra large button</button></p>
<p><button class="btn btn-black btn-block">The block button</button></p>
{% endexample %}

### Disabled

Here are the disabled looks for the buttons:

{% example html %}
<p><button class="btn" disabled>The default button</button></p>
<p><button class="btn btn-primary" disabled>The primary button</button></p>
<p><button class="btn btn-secondary" disabled>The secondary button</button></p>
<p><button class="btn btn-tertiary" disabled>The tertiary button</button></p>
<p><button class="btn btn-white" disabled>The white button</button></p>
<p><button class="btn btn-gray-light" disabled>The gray-light button</button></p>
<p><button class="btn btn-gray" disabled>The gray button</button></p>
<p><button class="btn btn-gray-dark" disabled>The gray-dark button</button></p>
{% endexample %}

### Icons

{{ site.projectname }} uses Entypo+ for icons.

{% example html %}
<p><button class="btn btn-black btn-sm"><span class="icon">{% icon info-with-circle.svg %}</span> The small button</button></p>
<p><button class="btn btn-black"><span class="icon">{% icon info-with-circle.svg %}</span> The default button</button></p>
<p><button class="btn btn-black btn-lg"><span class="icon">{% icon info-with-circle.svg %}</span> The large button</button></p>
<p><button class="btn btn-black btn-xl"><span class="icon">{% icon info-with-circle.svg %}</span> The extra large button</button></p>
<p><button class="btn btn-primary btn-block"><span class="icon">{% icon info-with-circle.svg %}</span> The block button</button></p>
{% endexample %}
