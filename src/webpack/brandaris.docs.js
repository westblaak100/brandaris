// Webpack should also output CSS based on the following file
import "./../docs/scss/brandaris.docs.scss";

// JavaScripts
var async = require("async");
var tocbot = require("tocbot");

import "simple-jekyll-search";

var anchorJS = require("anchor-js");
var anchors = new anchorJS();
anchors.add(".content h1, .content h2, .content h3, .content h4, .content h5, .content h6").remove(".docs-example h1, .docs-example h2, .docs-example h3, .docs-example h4, .docs-example h5, .docs-example h6");

// tocbot
var exampleHeadings = document.querySelectorAll(".docs-example h3, .docs-example h4, .docs-example h5");

async.each(exampleHeadings, function(heading, callback) {
  heading.classList.add("js-toc-ignore");
  callback();
}, function() {
  tocbot.init({
    tocSelector: ".tocbot",
    contentSelector: ".content",
    headingSelector: "h3, h4, h5",
    positionFixedSelector: ".tocbot-wrap"
  });
});

// search
var input = document.getElementById("search-input"),
  results = document.getElementById("results-container");

var jsonStringEnv = "/docs/search.json";

if(process.env.NODE_ENV === "production") {
  jsonStringEnv = "/brandaris" + jsonStringEnv;
}

/* eslint-disable no-undef */
SimpleJekyllSearch.init({
  searchInput: input,
  resultsContainer: results,
  searchResultTemplate: "<li><a href=\"{url}\">{title}</a></li>",
  noResultsText: "<li>Sorry, there are no results for that search.</li>",
  json: jsonStringEnv
});
/* eslint-enable */

input.addEventListener("input", showHideSearch);

function showHideSearch() {
  if(!results.hasChildNodes() || input.value.length === 0) {
    results.parentNode.classList.add("hide");
  } else {
    results.parentNode.classList.remove("hide");
  }
}
