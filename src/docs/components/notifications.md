---
layout: docs
title: Notifications
group: components
---

Sometimes you need to grab someone's attention. You need something good. Something they see. Something they remember when they wake up in the middle of the night. Something [special](https://www.youtube.com/watch?v=dQw4w9WgXcQ).

### User friendliness
With great notifications comes great responsibility. Here is some guidence to help you make your new notifications really user friendly.

#### Clear question and options
You should use a clear question or statement with an explanation in the content area, such as "Erase your storage?" or "Delete your account?" In general, you should avoid apologies, ambiguity, or questions, such as "Warning!" or "Are you sure?".

Avoid presenting users with ambiguous or unclear options. You should use only clear options. In most cases users should be able to understand the choices based on the title and button text alone.

#### Provide important information
It’s important that a dialog doesn’t obscure information that might be useful for users. For example, a dialog asking users to confirm the deletion of some items should list the items being deleted.

### How to use
<button class="btn btn-black" data-notification="notification-example-1">Trigger notification as pop-up</button>

<div class="box notification hide mb-2" id="notification-example-1" data-dismissable="true">
  <div class="box-header bg-primary">
    <span class="h4 clr-white"><span class="icon clr-white">{% icon info-with-circle.svg %}</span> This is the awesome info notification title</span>
  </div>
  <div class="box-content pt-2">
    <p>Insert your nice lorem ipsum here.</p>
  </div>
  <div class="box-footer bg-gray-light align-right">
    <button class="btn btn-gray-dark" data-dismiss="notification">Disagree</button>
    <button class="btn btn-primary">Agree</button>
  </div>
</div>

{% highlight html %}
<button class="btn btn-black" data-notification="notification-example-1">Trigger notification as pop-up</button>

<div class="box notification hide mb-2" id="notification-example-1" data-dismissable="true">
  <div class="box-header bg-primary">
    <span class="h4 clr-white"><span class="icon clr-white">{% raw %}{% icon info-with-circle.svg %}{% endraw %}</span> This is the awesome info notification title</span>
  </div>
  <div class="box-content pt-2">
    <p>Insert your nice lorem ipsum here.</p>
  </div>
  <div class="box-footer bg-gray-light align-right">
    <button class="btn btn-gray-dark" data-dismiss="notification">Disagree</button>
    <button class="btn btn-primary">Agree</button>
  </div>
</div>
{% endhighlight %}

### Examples
We <span class="clr-tertiary">&hearts;</span> copy-pasting! Here you go, some info, warning and danger notifications ready to go.

#### Info
<div class="box notification notification-inline bg-primary mb-2">
  <p class="clr-white"><span class="icon clr-white">{% icon info-with-circle.svg %}</span> Insert your nice lorem ipsum here.</p>
</div>

{% highlight html %}
<div class="box notification notification-inline bg-primary mb-2">
  <p class="clr-white"><span class="icon clr-white">{% raw %}{% icon info-with-circle.svg %}{% endraw %}</span> Insert your nice lorem ipsum here.</p>
</div>
{% endhighlight %}

<div class="box notification mb-2">
  <div class="box-header bg-primary">
    <span class="h4 clr-white"><span class="icon clr-white">{% icon info-with-circle.svg %}</span> This is the awesome info notification title</span>
  </div>
  <div class="box-content pt-2">
    <p>Insert your nice lorem ipsum here.</p>
  </div>
  <div class="box-footer bg-gray-light align-right">
    <button class="btn btn-gray-dark">Disagree</button>
    <button class="btn btn-primary">Agree</button>
  </div>
</div>

{% highlight html %}
<div class="box notification mb-2">
  <div class="box-header bg-primary">
    <span class="h4 clr-white"><span class="icon clr-white">{% raw %}{% icon info-with-circle.svg %}{% endraw %}</span> This is the awesome info notification title</span>
  </div>
  <div class="box-content pt-2">
    <p>Insert your nice lorem ipsum here.</p>
  </div>
  <div class="box-footer bg-gray-light align-right">
    <button class="btn btn-gray-dark">Disagree</button>
    <button class="btn btn-primary">Agree</button>
  </div>
</div>
{% endhighlight %}

#### Warning
<div class="box notification notification-inline bg-secondary mb-2">
  <p><span class="icon">{% icon warning.svg %}</span> Insert your nice lorem ipsum here.</p>
</div>

{% highlight html %}
<div class="box notification notification-inline bg-secondary mb-2">
  <p><span class="icon">{% raw %}{% icon warning.svg %}{% endraw %}</span> Insert your nice lorem ipsum here.</p>
</div>
{% endhighlight %}

<div class="box notification mb-2">
  <div class="box-header bg-secondary">
    <span class="h4"><span class="icon">{% icon warning.svg %}</span> This is the awesome warning notification title</span>
  </div>
  <div class="box-content pt-2">
    <p>Insert your nice lorem ipsum here.</p>
  </div>
  <div class="box-footer bg-gray-light align-right">
    <button class="btn btn-gray">Cancel</button>
    <button class="btn btn-secondary">OK</button>
  </div>
</div>

{% highlight html %}
<div class="box notification mb-2">
  <div class="box-header bg-secondary">
    <span class="h4"><span class="icon">{% raw %}{% icon warning.svg %}{% endraw %}</span> This is the awesome warning notification title</span>
  </div>
  <div class="box-content pt-2">
    <p>Insert your nice lorem ipsum here.</p>
  </div>
  <div class="box-footer bg-gray-light align-right">
    <button class="btn btn-gray">Cancel</button>
    <button class="btn btn-secondary">OK</button>
  </div>
</div>
{% endhighlight %}

#### Danger
<div class="box notification notification-inline bg-tertiary mb-2">
  <p class="clr-white"><span class="icon clr-white">{% icon new.svg %}</span> Insert your nice lorem ipsum here.</p>
</div>

{% highlight html %}
<div class="box notification notification-inline bg-tertiary mb-2">
  <p class="clr-white"><span class="icon clr-white">{% raw %}{% icon new.svg %}{% endraw %}</span> Insert your nice lorem ipsum here.</p>
</div>
{% endhighlight %}

<div class="box notification mb-2">
  <div class="box-header bg-tertiary">
    <span class="h4 clr-white"><span class="icon clr-white">{% icon new.svg %}</span> This is the awesome danger notification title</span>
  </div>
  <div class="box-content pt-2">
    <p>Insert your nice lorem ipsum here.</p>
  </div>
  <div class="box-footer bg-gray-light align-right">
    <button class="btn btn-gray-dark">Cancel</button>
    <button class="btn btn-tertiary">Delete</button>
  </div>
</div>

{% highlight html %}
<div class="box notification mb-2">
  <div class="box-header bg-tertiary">
    <span class="h4 clr-white"><span class="icon clr-white">{% raw %}{% icon new.svg %}{% endraw %}</span> This is the awesome danger notification title</span>
  </div>
  <div class="box-content pt-2">
    <p>Insert your nice lorem ipsum here.</p>
  </div>
  <div class="box-footer bg-gray-light align-right">
    <button class="btn btn-gray-dark">Cancel</button>
    <button class="btn btn-tertiary">Delete</button>
  </div>
</div>
{% endhighlight %}
