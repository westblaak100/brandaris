"use strict";

import { forEach } from "JS/functions";

var sliderTimer = [];

document.addEventListener("DOMContentLoaded", function() {
  // Grab all .sliders and .slider-navs
  var sliders = document.querySelectorAll(".slider"),
    sliderNavs = document.querySelectorAll(".slider-nav");

  // Autoplay all sliders
  forEach(sliders, function (slider, i) {
    var autoplay = slider.getAttribute("data-autoplay");

    // Check if slider needs autoplay
    if(autoplay === "true") {
      startTimer(i, slider.id);

      slider.addEventListener("mouseover", function() {
        stopTimer(sliderTimer[i]);
      });
      slider.addEventListener("mouseout", function() {
        startTimer(i,slider.id);
      });
    }
  });

  // Loop all .slider-navs
  forEach(sliderNavs, function (sliderNav) {
    var sliderID = sliderNav.getAttribute("data-slider-nav");

    // Listen to click event and slide
    forEach(sliderNav.children, function (child, j) {
      child.addEventListener("click", function() {
        var currentSlide = childWithClassIndex(sliderNav,"active");
        sliderSlide(sliderID, currentSlide, j);
      });
    });
  });

  function stopTimer(timer) {
    clearInterval(timer);
  }

  function startTimer(i,sliderID) {
    var slider = document.querySelectorAll("#"+ sliderID)[0];

    sliderTimer[i] = setInterval(function() {
      sliderSlide(sliderID,childWithClassIndex(slider,"active"));
    }, 5000);
  }

  // Slide the slider!
  function sliderSlide(sliderID, currentSlide, goToSlide) {
    var slider = document.querySelectorAll("#"+ sliderID)[0];

    if(goToSlide === undefined) {
      if((currentSlide + 1) < slider.children.length) {
        goToSlide = currentSlide + 1;
      } else {
        goToSlide = 0;
      }
    }

    // Only slide when goToSlide is different
    if(currentSlide !== goToSlide) {
      var sliderNav = document.querySelectorAll("*[data-slider-nav='"+ sliderID +"']")[0],
        newActiveSlide = document.querySelectorAll("#"+ sliderID + " div[data-slide='"+ goToSlide +"']")[0];

      // Remove .active class from all navigation items
      removeChildClass(sliderNav, "active");
      // Add .active class to navigation item
      sliderNav.querySelectorAll("[data-slide-go='"+ goToSlide +"']")[0].classList.add("active");

      // Remove .active slider class from all slides
      removeChildClass(slider, "active");
      // Add .active class
      newActiveSlide.classList.add("active");
    }
  }

  // Remove specific class from all element's children
  function removeChildClass(element, className) {
    forEach(element.children, function (el) {
      el.classList.remove(className);
    });
  }

  // Return the index for an element's child with a specific class
  function childWithClassIndex(element, className) {
    var childClass = false;
    forEach(element.children, function (el, i) {
      if(el.classList.contains(className)) {
        childClass = i;
      }
    });
    return childClass;
  }

});
