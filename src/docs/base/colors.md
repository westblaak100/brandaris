---
layout: docs
title: Colors
group: base
---

{{ site.projectname }} uses [Typesettings](http://typesettings.io/) for modular scale, vertical rhythm, and responsive ratio based headlines. By default it has a `6px` baseline. Furthermore, the following utilities can be used to add additional styles to texts.

### Background
Spice it up: add some color!

<div class="row clr-white align-center">
  <div class="col-xs">
    <div class="bg-primary"><br>Primary background<br><br></div>
  </div>
  <div class="col-xs">
    <div class="bg-secondary"><br>Secondary background<br><br></div>
  </div>
  <div class="col-xs">
    <div class="bg-tertiary"><br>Tertiary background<br><br></div>
  </div>
</div>
<br>
<div class="row align-center">
  <div class="col-xs">
    <div class="bg-quaternary clr-white"><br>Quaternary background<br><br></div>
  </div>
  <div class="col-xs">
    <div class="bg-gray-light"><br>Gray-light background<br><br></div>
  </div>
  <div class="col-xs">
    <div class="bg-gray"><br>Gray background<br><br></div>
  </div>  
</div>
<br>
<div class="row align-center">
  <div class="col-xs">
    <div class="bg-gray-dark clr-white"><br>Gray-dark background<br><br></div>
  </div>
  <div class="col-xs">
    <div class="bg-white"><br>White background<br><br></div>
  </div>
  <div class="col-xs">
    <div class="bg-black clr-white"><br>Black background<br><br></div>
  </div>
</div>
<br>

{% highlight html %}
<div class="row">
  <div class="col-xs">
    <div class="bg-primary">...</div>
  </div>
  <div class="col-xs">
    <div class="bg-secondary">...</div>
  </div>
  <div class="col-xs">
    <div class="bg-tertiary">...</div>
  </div>
</div>

<div class="row">
  <div class="col-xs">
    <div class="bg-gray-light">...</div>
  </div>
  <div class="col-xs">
    <div class="bg-gray">...</div>
  </div>
  <div class="col-xs">
    <div class="bg-gray-dark">...</div>
  </div>
</div>

<div class="row">
  <div class="col-xs">
    <div class="bg-white">...</div>
  </div>
  <div class="col-xs">
    <div class="bg-black">...</div>
  </div>
</div>
{% endhighlight %}

### Text colors

Change the text color.

{% example html %}
<p class="clr-primary">You can easily change the text color.</p>
<p class="clr-secondary">You can easily change the text color.</p>
<p class="clr-tertiary">You can easily change the text color.</p>
<p class="clr-primary">You can easily change the <a href="#" class="clr-white">text</a> color.</p>
<p class="clr-tertiary">You can easily change the <span class="clr-black">text</span> color.</p>
<p class="clr-quaternary">You can easily change the <span class="clr-black">text</span> color.</p>
<p class="clr-gray-light">You can easily change the text color.</p>
<p class="clr-gray">You can easily change the text color.</p>
<p class="clr-gray-dark">You can easily change the text color.</p>
{% endexample %}
