---
layout: docs
title: Charts
group: components
---

{{ site.projectname }} uses [Chart.js](http://www.chartjs.org/) for rendering Charts. Because the JavaScript for charts is a bit fat you must only load it on the pages that have charts. You can do this by adding the following snippets:

**With time axis**

Load [moment.js](https://momentjs.com/) as well.
```
<script src='{{ site.baseurl }}/assets/scripts/moment.min.js'></script>
<script src='{{ site.baseurl }}/assets/scripts/chart.min.js'></script>
```
{: .mb-2 }

**Without time axis**

```
<script src='{{ site.baseurl }}/assets/scripts/chart.min.js'></script>
```
{: .mb-2 }

### Default usage
A lot of different [settings](http://www.chartjs.org/docs/latest/) can be applied to the different charts ([`line`](http://www.chartjs.org/docs/latest/charts/line.html), [`bar`](http://www.chartjs.org/docs/latest/charts/bar.html), [`radar`](http://www.chartjs.org/docs/latest/charts/radar.html), [`polar area`](http://www.chartjs.org/docs/latest/charts/polar.html), [`doughnut and pie`](http://www.chartjs.org/docs/latest/charts/doughnut.html) and [`bubble`](http://www.chartjs.org/docs/latest/charts/bubble.html)).

<div class="chart-container mb-2" data-source="{{ site.baseurl }}/docs/assets/charts/chart.default.json">
  <canvas id="chart-default"></canvas>
</div>
{% highlight html %}
<div class="chart-container" data-source="{{ site.baseurl }}/docs/assets/charts/chart.default.json">
  <canvas id="chart-default"></canvas>
</div>
{% endhighlight %}

Where `chart.default.json` contains the following:
{% highlight json %}
{
  "type": "bar",
  "data": {
    "labels": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
    "datasets": [
      {
        "label": "Example data 1",
        "data": [65, 59, 20, 81, 56, 55, 40],
        "borderWidth": 2,
        "color": "primary"
      }, {
        "label": "Example data 2",
        "data": [20, 81, 65, 59, 32, 23, 80],
        "borderWidth": 2,
        "color": "secondary"
      }
    ]
  },
  "options": {
    "scales": {
      "yAxes": [
        {
          "gridLines": {
            "display": true
          }
        }
      ],
      "xAxes": [
        {
          "gridLines": {
            "display": false
          }
        }
      ]
    }
  }
}
{% endhighlight %}

Pay attention to the `color` option, as it's different from the default Chart.js settings. It will insert all needed colors based on the set [framework color](/base/colors/).

### Examples

#### Bubble
<div class="chart-container mb-2" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.bubble.json">
  <canvas id="chart-example-bubble"></canvas>
</div>
{% highlight html %}
<div class="chart-container" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.bubble.json">
  <canvas id="chart-example-bubble"></canvas>
</div>
{% endhighlight %}

{% highlight json %}
{
  "type": "bubble",
  "data": {
    "datasets": [
      {
        "label":"Bubble example data",
        "data": [
          {
            "x": 4,
            "y": 8,
            "r": 32
          }, {
            "x": -3,
            "y": -1,
            "r": 15
          }
        ],
        "color": "primary"
      }
    ]
  },
  "options": {
    "scales": {
      "yAxes": [
        {
          "gridLines": {
            "display": true
          },
          "ticks": {
            "suggestedMin": -5,
            "suggestedMax": 15
          }
        }
      ],
      "xAxes": [
        {
          "gridLines": {
            "display": true
          },
          "ticks": {
            "suggestedMin": -10,
            "suggestedMax": 10
          }
        }
      ]
    }
  }
}
{% endhighlight %}

#### Doughnut
<div class="chart-container mb-2" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.doughnut.json">
  <canvas id="chart-example-doughnut"></canvas>
</div>
{% highlight html %}
<div class="chart-container" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.doughnut.json">
  <canvas id="chart-example-doughnut"></canvas>
</div>
{% endhighlight %}

{% highlight json %}
{
  "type": "doughnut",
  "data": {
    "labels":["One","Two","Three"],
    "datasets": [
      {
        "data": [300,50,100],
        "color": [
          "primary",
          "secondary",
          "tertiary"
        ]
      }
    ]
  },
  "options": {}
}
{% endhighlight %}

#### Line
<div class="chart-container mb-2" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.line.json">
  <canvas id="chart-example-line"></canvas>
</div>
{% highlight html %}
<div class="chart-container" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.line.json">
  <canvas id="chart-example-line"></canvas>
</div>
{% endhighlight %}

{% highlight json %}
{
  "type": "line",
  "data": {
    "labels": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
    "datasets": [
      {
        "label": "Example data 1",
        "data": [65, 59, 20, 81, 56, 55, 40],
        "borderWidth": 2,
        "color": "primary"
      }, {
        "label": "Example data 2",
        "data": [20, 81, 65, 59, 32, 23, 80],
        "borderWidth": 2,
        "color": "secondary"
      }
    ]
  },
  "options": {}
}
{% endhighlight %}

#### Pie
<div class="chart-container mb-2" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.pie.json">
  <canvas id="chart-example-pie"></canvas>
</div>
{% highlight html %}
<div class="chart-container" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.pie.json">
  <canvas id="chart-example-pie"></canvas>
</div>
{% endhighlight %}

{% highlight json %}
{
  "type": "pie",
  "data": {
    "labels":["One","Two","Three","Four"],
    "datasets": [
      {
        "data": [200,100,100,200],
        "color": [
          "primary",
          "secondary",
          "tertiary",
          "quaternary"
        ]
      }
    ]
  },
  "options": {}
}
{% endhighlight %}

#### Polar
<div class="chart-container mb-2" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.polar.json">
  <canvas id="chart-example-polar"></canvas>
</div>
{% highlight html %}
<div class="chart-container" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.polar.json">
  <canvas id="chart-example-polar"></canvas>
</div>
{% endhighlight %}

{% highlight json %}
{
  "type": "polarArea",
  "data": {
    "labels":["One","Two","Three","Four"],
    "datasets": [
      {
        "data": [50,100,300,200],
        "color": [
          "primary",
          "secondary",
          "tertiary",
          "quaternary"
        ]
      }
    ]
  },
  "options": {}
}
{% endhighlight %}

#### Radar
<div class="chart-container mb-2" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.radar.json">
  <canvas id="chart-example-radar"></canvas>
</div>
{% highlight html %}
<div class="chart-container" data-source="{{ site.baseurl }}/docs/assets/charts/chart.example.radar.json">
  <canvas id="chart-example-radar"></canvas>
</div>
{% endhighlight %}

{% highlight json %}
{
  "type": "radar",
  "data": {
    "labels": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
    "datasets": [
      {
        "label": "Example data 1",
        "data": [65, 59, 20, 81, 56, 55, 40],
        "borderWidth": 2,
        "color": "primary"
      }, {
        "label": "Example data 2",
        "data": [20, 81, 65, 59, 32, 23, 80],
        "borderWidth": 2,
        "color": "secondary"
      }
    ]
  },
  "options": {}
}
{% endhighlight %}
