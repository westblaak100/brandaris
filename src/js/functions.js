"use strict";

export function forEach(array, callback, scope) {
  for (var i = 0, len = array.length; i < len; i++) {
    callback.call(scope || this, array[i], i, array); // passes back stuff we need - use the same arguments as standard Array.prototype.forEach
  }
}
