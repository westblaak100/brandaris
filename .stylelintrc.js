module.exports = {
  "extends": "stylelint-config-recommended-scss",
  "rules": {
    // There must always be an empty line before at-rules.
    "at-rule-empty-line-before": ["always", {
      // Allow mixins to have an empty line before
      "ignore": ["after-comment", "first-nested"],
      "ignoreAtRules": ["import", "else"],
    }],

    // Specify lowercase for at-rules names.
    "at-rule-name-case": "lower",

    // There must always be a newline after the closing brace.
    "block-closing-brace-newline-after": ["always", {
      "ignoreAtRules": ["if", "else"],
    }],

    // There must always be a newline after the opening brace.
    "block-opening-brace-newline-after": "always",

    // There must always be a single space before the opening brace.
    "block-opening-brace-space-before": "always",

    // Specify lowercase for hex colors.
    "color-hex-case": "lower",

    // Specify long notation for hex colors.
    "color-hex-length": "long",

    // Disallow invalid hex colors.
    "color-no-invalid-hex": true,

    // There must always be an empty line before comments.
    "comment-empty-line-before": ["always", {
      "except": ["first-nested"],
      "ignore": ["stylelint-commands"],
    }],

    // Require or disallow whitespace on the inside of comment markers.
    "comment-whitespace-inside": "always",

    // Disallow duplicate properties within declaration blocks.
    "declaration-block-no-duplicate-properties": true,

    // There must always be a newline after the semicolon.
    "declaration-block-semicolon-newline-after": "always",

    // Maximum number of declarations allowed.
    "declaration-block-single-line-max-declarations": 1,

    // There must always be a trailing semicolon.
    "declaration-block-trailing-semicolon": "always",

    // Force space after ":"
    "declaration-colon-space-after": "always",

    // Disallow space before ":"
    "declaration-colon-space-before": "never",

    // Specify lowercase for function names. Camel case function names, e.g.
    // translateX, are accounted for when the lower option is used.
    "function-name-case": "lower",

    // we use soft tabs (2 spaces)
    "indentation": 2,

    // Disallow units for zero lengths.
    "length-zero-no-unit": true,

    // Limit the allowed nesting depth.
    "max-nesting-depth": [3, {
      "ignore": ["blockless-at-rules"],
      // ignore @if (scss)
      "ignoreAtRules": ["if"]
    }],

    // Allow selectors of lower specificity from coming after overriding
    // selectors of higher specificity.
    // Overwrites stylelint-config-recommended-scss
    "no-descending-specificity": null,

    // Disallow duplicate selectors within a stylesheet.
    "no-duplicate-selectors": true,

    // no end of line whitespace allowed
    "no-eol-whitespace": true,

    // Disallow trailing zeros in numbers.
    "number-no-trailing-zeros": true,

    // Specify lowercase for properties.
    "property-case": "lower",

    // Sass/SCSS-like function naming should be kabab-cased.
    "scss/at-function-pattern": ["^([a-z][a-z0-9]*)(-[a-z0-9]+)*$", {
      "message": "Functions should be kebab-case.",
    }],

    // Sass/SCSS-like mixin naming should be kabab-cased.
    "scss/at-mixin-pattern": ["^([a-z][a-z0-9]*)(-[a-z0-9]+)*$", {
      "message": "Mixins should be kebab-case.",
    }],

    // Selectors should be kebab-case and JavaScript hooks (.js-*)
    // should not have styles.
    "selector-class-pattern": ["^(?!js-.+)(([a-z][a-z0-9]*)(-[a-z0-9]+)*$)", {
      "message": "Selectors should be kebab-case and JavaScript hooks (.js-*) should not have styles."
    }],

    // There must always be a newline after the commas.
    "selector-list-comma-newline-after": "always",

    // Maximum compound selectors allowed.
    "selector-max-compound-selectors": 3,

    // No ID selectors allowed
    "selector-max-id": 0,

    // Disallow redundant values in shorthand properties.
    "shorthand-property-no-redundant-values": true,

    // Specify double quotes around strings.
    "string-quotes": "double",

    // Disallow vendor prefix, they are added by autoprefixer
    "value-no-vendor-prefix": true,
  },
  "plugins": [
    "stylelint-scss"
  ]
}
