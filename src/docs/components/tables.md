---
layout: docs
title: Tables
group: components
---

{{ site.projectname }} uses a clean and simple styling for tables. All styles can be easily combined to display your ultimate table.

### Default table

Using the most basic table markup, here's how `.table`-based tables look in {{ site.projectname }}.

{% example html %}
<table class="table mb-2">
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Jack</td>
      <td>Smith</td>
      <td>j.smith</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Eve</td>
      <td>Ning</td>
      <td>e.ning</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Lee</td>
      <td>Amos</td>
      <td>l.amos</td>
    </tr>
  </tbody>
</table>
{% endexample %}

### Borderless table

Don't you like borders? Just add `.table-borderless` and they will be gone.

{% example html %}
<table class="table table-borderless mb-2">
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Jack</td>
      <td>Smith</td>
      <td>j.smith</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Eve</td>
      <td>Ning</td>
      <td>e.ning</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Lee</td>
      <td>Amos</td>
      <td>l.amos</td>
    </tr>
  </tbody>
</table>
{% endexample %}

### Striped table

Large tables are easier to parse visually if rows are easily distinguishable. You can add striped rows by adding `.table-striped` to your `.table`-based tables.

{% example html %}
<table class="table table-striped mb-2">
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Jack</td>
      <td>Smith</td>
      <td>j.smith</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Eve</td>
      <td>Ning</td>
      <td>e.ning</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Lee</td>
      <td>Amos</td>
      <td>l.amos</td>
    </tr>
  </tbody>
</table>
{% endexample %}

### Full width table

Sometimes you want a table to cover the full width of their parent element. You can acomplish this by adding `.table-full` to your `.table`-based tables.

{% example html %}
<table class="table table-full mb-2">
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Jack</td>
      <td>Smith</td>
      <td>j.smith</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Eve</td>
      <td>Ning</td>
      <td>e.ning</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Lee</td>
      <td>Amos</td>
      <td>l.amos</td>
    </tr>
  </tbody>
</table>
{% endexample %}

### Hoverable table

You can easily go trough tables when you can hover them. Add `.table-hover` to your `.table`-based tables to get hoverable tables.

{% example html %}
<table class="table table-hover mb-2">
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Jack</td>
      <td>Smith</td>
      <td>j.smith</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Eve</td>
      <td>Ning</td>
      <td>e.ning</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Lee</td>
      <td>Amos</td>
      <td>l.amos</td>
    </tr>
  </tbody>
</table>
{% endexample %}

### Colored table

Make your tables sparkle with `.table-colored`!

{% example html %}
<table class="table table-colored mb-2">
  <thead>
    <tr>
      <th>#</th>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Username</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>Jack</td>
      <td>Smith</td>
      <td>j.smith</td>
    </tr>
    <tr>
      <th>2</th>
      <td>Eve</td>
      <td>Ning</td>
      <td>e.ning</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Lee</td>
      <td>Amos</td>
      <td>l.amos</td>
    </tr>
  </tbody>
</table>
{% endexample %}

### Responsive tables

You can wrap your tables in `.table-responsive` for responsive tables.

<div class="table-responsive mb-2">
  <table class="table table-full">
    <thead>
      <tr>
        <th>#</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Username</th>
      	<th>Phone</th>
      	<th>E-mail</th>
      	<th>Street address</th>
      	<th>City</th>
      	<th>Postal / Zip</th>
      	<th>Country</th>
      	<th>Company</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>1</th>
        <td>Jack</td>
        <td>Smith</td>
        <td>j.smith</td>
      	<td>+31397406593</td>
      	<td>pretium.et@eratvitaerisus.edu</td>
      	<td>739-9801 Class St.</td>
      	<td>Zittau</td>
      	<td>3955</td>
      	<td>Swaziland</td>
      	<td>Dapibus Id Blandit PC</td>
      </tr>
      <tr>
        <th>2</th>
        <td>Eve</td>
        <td>Ning</td>
        <td>e.ning</td>
      	<td>+31971126991</td>
      	<td>sagittis.Duis.gravida@vitae.ca</td>
      	<td>P.O. Box 745, 3358 Consequat St.</td>
      	<td>Eindhoven</td>
      	<td>45838</td>
      	<td>Germany</td>
      	<td>Risus Nunc PC</td>
      </tr>
      <tr>
        <th>3</th>
        <td>Lee</td>
        <td>Amos</td>
        <td>l.amos</td>
      	<td>+31935644470</td>
      	<td>dolor.vitae@ornaretortorat.co.uk</td>
      	<td>788-6718 Pede Rd.</td>
      	<td>Upper Hutt</td>
      	<td>0599HK</td>
      	<td>Peru</td>
      	<td>Blandit Consulting</td>
      </tr>
    </tbody>
  </table>
</div>

{% highlight html %}
<div class="table-responsive mb-2">
  <table class="table table-full">
    ...
  </table>
</div>
{% endhighlight %}
