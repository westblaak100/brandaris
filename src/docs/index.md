---
layout: docs
title: Getting started
---

{{ site.projectname }} is a mobile first framework, which has a flexible [grid]({{ site.baseurl }}/base/grid/), with vertical rythm, modular scale, and responsive ratio based headlines for [typography]({{ site.baseurl }}/base/typography/). It is extended with the [Entypo+](http://www.entypo.com/) SVG icons from Daniel Bruce and easy to use, thanks to this automatically build (thanks [Jekyll](https://jekyllrb.com/)) documentation.

### System Preparation

#### Required tools

To use the front end develop environment for the projects, you'll need the following things installed on your machine.

1. [NodeJS](http://nodejs.org){:target="_blank"} - use the installer.
2. [Ruby](https://www.ruby-lang.org/){:target="_blank"} - use an installer like [RubyInstaller](https://rubyinstaller.org/){:target="_blank"}.
3. [Jekyll](http://jekyllrb.com/){:target="_blank"} - `$ gem install jekyll jekyll-sitemap kramdown rouge`
4. [GulpJS](https://github.com/gulpjs/gulp){:target="_blank"} - `$ npm install -g gulp`

#### Recommended tools

- [Atom](https://atom.io/){:target="_blank"}<br>Atom is a fast, goodlooking and extendable text editor by GitHub.
- [Cmdr](http://cmder.net/){:target="_blank"}<br>Console emulator for Windows, based on [Conemu](https://conemu.github.io/). For context menu integration open terminal in installation dir (preferably `C:\cmder`) as Administrator and run `.\cmder.exe /REGISTER ALL`.<br>
   _Mini ([download](https://github.com/cmderdev/cmder/releases/latest)) version should be enough._

### Local Installation

After installing required tools, run the following command inside the `{{ site.projectname }}` directory:

{% highlight shell %}
$ npm install
{% endhighlight %}

### Usage

Run this simple command for more information about available tasks and options.

{% highlight shell %}
$ gulp help
{% endhighlight %}

### HTML template

Often you need an empty HTML template. You can steal this one, I won't tell.

{% highlight html %}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<!-- Page description -->">
    <meta name="author" content="<!-- Your name -->">

    <title><!-- Page title --></title>

    <link href="/assets/styles/main.css" rel="stylesheet">
    <link rel="apple-touch-icon" href="/assets/images/apple-touch-icon.png">
    <link rel="icon" href="/favicon.ico">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="<!-- Site name -->">
    <meta property="og:title" content="<!-- Page title -->">
    <meta property="og:description" content="<!-- Page description -->">
    <meta property="og:url" content="<!-- Page url -->">
    <meta property="og:image" content="<!-- Page image -->">
  </head>
  <body>
    <!-- Content -->
  </body>
</html>
{% endhighlight %}
