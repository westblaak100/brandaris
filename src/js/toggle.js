"use strict";

import { forEach } from "JS/functions";

export function toggleListener() {
  var _self = this;
  var target = document.getElementById(_self.getAttribute("data-target")),
    altText = _self.getAttribute("data-alttext"),
    contentText = _self.innerHTML;

  if (altText) {
    _self.setAttribute("data-alttext", contentText);
    _self.innerHTML = altText;
  } else {
    _self.innerHTML = contentText;
  }
  target.classList.toggle("hide");
}

export function checkToggle() {
  var _self = this;
  var targets = document.querySelectorAll("." + _self.getAttribute("data-target"));

  forEach(targets, function (target) {
    target.checked = _self.checked;
  });
}

export function markToggleIfAllChildrenChecked(toggle) {
  var targets = document.querySelectorAll("." + toggle.getAttribute("data-target"));
  var allChecked = true;
  var allUnchecked = true;
  forEach(targets, function (target) {
    allChecked = allChecked && target.checked;
    allUnchecked = allUnchecked && !target.checked;
  });
  if (allChecked){
    toggle.checked = true;
    toggle.indeterminate = false;
  } else if (allUnchecked){
    toggle.checked = false;
    toggle.indeterminate = false;
  } else {
    toggle.checked = false;
    toggle.indeterminate = true;
  }
}

export function findAndMarkToggleAll() {
  var _self = this;
  var toggleAllElement = _self;
  while (toggleAllElement && toggleAllElement.classList && !toggleAllElement.classList.contains("dataToggleGroup")){
    toggleAllElement = toggleAllElement.parentNode;
  }
  var toggleInput = toggleAllElement.querySelector(".dataToggleAll");
  markToggleIfAllChildrenChecked(toggleInput);
}

export function attachListenerToChildToggles(parentToggle) {
  var targets = document.querySelectorAll("." + parentToggle.getAttribute("data-target"));
  forEach(targets, function (target) {
    target.addEventListener("click", findAndMarkToggleAll);
  });
}

export function initToggle() {
  // Show / hide
  var toggleElements = document.getElementsByClassName("dataToggle");

  forEach(toggleElements, function (toggle) {
    toggle.addEventListener("click", toggleListener);
  });

  var toggleAllElements = document.getElementsByClassName("dataToggleAll");

  forEach(toggleAllElements, function (toggle) {
    toggle.addEventListener("click", checkToggle);
    markToggleIfAllChildrenChecked(toggle);
    attachListenerToChildToggles(toggle);
  });
}

initToggle();
