---
layout: docs
title: Icons
group: components
---

{{ site.projectname }} comes with the beautiful buttons [Entypo+](http://www.entypo.com/) from Daniel Bruce.

### How to use

You should use the icons inline. With Jekyll you can easily do this like `<span class="icon">{% raw %}{% icon info-with-circle.svg %}{% endraw %}</span>`. If you're using plain HTML you can copy the specific icon data from the `_icons` directory and paste it inside a `<span>` element with class `.icon`.

{% highlight html %}
<span class="icon">{% icon info-with-circle.svg %}</span>
{% endhighlight %}

#### Button

{% example html %}
<p><button class="btn"><span class="icon">{% icon info-with-circle.svg %}</span> A button with icon</button></p>
{% endexample %}

#### Color
You can colorize the icons by adding the [color classes]({{ site.baseurl }}/base/colors/#text-colors).

#### Size
You can controll the size of the icons by adding a sizing class like `.small`, `.h1`, `.h2`, `.h3`, `.h4`, `.h5` and `.h6`.

### Entypo+
Here you can see all the included Entypo+ icons.

{% if site['ismodule'] %}
  {% assign iconSource = "../" | append: site['icons'] %}
{% else %}
  {% assign iconSource = site['icons'] | replace: site['source'], '' %}
{% endif %}

<div class="row align-center">
  {% directory path: iconSource %}
    <div class="col-4 col-sm-3 col-md-2">
      <p>
        <span class="icon h3">{% icon {{ file.name }} %}</span><br>
        <small>{{ file.name | remove: ".svg" }}</small>
      </p>
    </div>
  {% enddirectory %}
</div>
