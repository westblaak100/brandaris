---
layout: default
title: Brandaris · Front end awesomeness, true story
---

<section class="content container-fluid full-screen hero mb-0">
  <div id="top" class="row center-xs middle-xs">
    <header class="col-10 clr-white" role="banner">
      <br><br>
      <img src="{{ site.baseurl }}/assets/images/brandaris_logo.svg" alt="Brandaris Logo" width="280">
      <h1 class="uppercase">Brandaris</h1>
      <p class="lowercase lg"><em>Front end awesomeness, true story</em></p>
      <a href="https://bitbucket.org/maartenbrakkee/brandaris/get/master.zip" class="btn btn-lg"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.505 8.698L10 11 7.494 8.698a.512.512 0 0 0-.718 0 .5.5 0 0 0 0 .71l2.864 2.807a.51.51 0 0 0 .717 0l2.864-2.807a.498.498 0 0 0 0-.71.51.51 0 0 0-.716 0zM10 .4A9.6 9.6 0 0 0 .4 10c0 5.303 4.298 9.6 9.6 9.6s9.6-4.297 9.6-9.6A9.6 9.6 0 0 0 10 .4zm0 17.954A8.353 8.353 0 0 1 1.646 10 8.354 8.354 0 1 1 10 18.354z"/></svg></span> Download</a> <a href="{{ site.baseurl }}/docs/" class="btn btn-lg btn-primary"><span class="icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M16 1H4a1 1 0 0 0-1 1v16a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zm-1 16H5V3h10v14zM13 5H7v2h6V5zm0 8H7v2h6v-2zm0-4H7v2h6V9z"/></svg></span> Documentation</a>
    </header>
  </div>
</section>
