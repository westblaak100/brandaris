---
layout: docs
title: Boxes
group: components
---

These are boxes.

### Default

{% example html %}
<div class="box mb-2">
 <span>This is a box</span>
</div>
{% endexample %}

### Full height box

This box can be used in combination with a `.full` class on a row to make all boxes full height.

{% example html %}
<div class="row full mb-2">
  <div class="col-6">
    <div class="box box-full bg-primary">
     <span>This is a full heigth box</span>
    </div>
  </div>
  <div class="col-6">
    <div class="box box-full bg-secondary">
     <span>This is a full height box</span>
    </div>
  </div>
</div>
{% endexample %}

### Headers and footers

Need a header or footer to make a box special?

{% example html %}
<div class="box bg-gray-light mb-2">
  <div class="box-header bg-primary">
    <span>The box header</span>
  </div>
  <div class="py-2">
    <span>The box content</span>
  </div>
  <div class="box-footer bg-primary">
    <span>The box footer</span>
  </div>
</div>
{% endexample %}

#### Header image

{% example html %}
<div class="box mb-2">
  <div class="box-header bg-image">
    <div>
      <img src="/assets/images/repository.jpg" alt="">
    </div>
    <div class="row bottom">
      <div class="col mb-2">
        <span>The box header</span>
      </div>
    </div>
  </div>
  <div class="pt-2 pb-2">
    <span>The box content</span>
  </div>
  <div class="box-footer bg-primary">
    <span>The box footer</span>
  </div>
</div>
{% endexample %}

### Split

Split the box in multiple parts.

{% example html %}
<div class="box mb-2">
  <div class="box-header bg-primary">
    <span>Header content</span>
  </div>
  <div class="box-split pt-2">
    <span>The box content</span>
  </div>
  <div class="box-split pt-2">
    <span>The box content</span>
  </div>
  <div class="box-split pt-2">
    <span>The box content</span>
  </div>
  <div class="box-footer bg-gray-light">
    <span>Footer content</span>
  </div>
</div>
{% endexample %}
