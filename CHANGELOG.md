# Changelog
All notable changes to this project will be documented in this file.

## [5.0.1-5.0.6] - 2018-05-16
### Fixed
- `plumber-sass` was not included in package.json
- Fixed default button line-height and padding (`rem` instead of `em`)
- Fix `label`s and form help text
- Equal hieght for `select` and file-`input` and `inputs`
- Fix static input height

## [5.0.0-beta6] - 2018-05-16
### Fixed
- Fix line heights multiple line buttons.

## [5.0.0-beta5] - 2018-05-08
### Fixed
- Fix no plumber on buttons and inputs.

## [5.0.0-beta4] - 2018-05-08
### Fixed
- Fix for `.bg-image:last-child` width.
- Fix CSS `.row > *` order related to `.col`-classes.

### Removed
- Padding on boxes for devices `< xs`.

## [5.0.0-beta3] - 2018-05-07
### Fixed
- Only set `font-size` and `line-height` for `body` direct children.

## [5.0.0-beta2] - 2018-05-07
### Fixed
- `.container` max-width improved.

### Removed
- `$mar-size-mobile` and `$pad-size-mobile` are removed in favor of `$spacer`.

## [5.0.0-beta] - 2018-03-28
### Added
- New (responsive) spacing classes, see docs for usage information. To search for combinations of new padding and margin classes, `class="[^.]+(pt|pl|pr|pb)[^.]+(pb|pr|pl|pt)[^.]+"` or `class="[^.]+(mt|ml|mr|mb)[^.]+(mb|mr|ml|mt)[^.]+"` can be used in [Atom](https://atom.io/) regex search.
- New (responsive) display classes, see docs for usage information.
- `$nav-btn-hover-bg` and `$nav-btn-hover-color` in favor of `$nav-btn-hover-style`.
- [Plumber](https://github.com/jamonserrano/plumber-sass) for vertical (understandable) rhythm.
- Extra grid size (`"xxs": 0`) for better class generation

### Fixed
- Grid moved to utils and refactored.

### Removed
- `$nav-btn-hover-style` in favor of `$nav-btn-hover-bg` and `$nav-btn-hover-color`.
- Removed the `:enabled` selector from `:hover` on `.btn`-classes
- `.hidden`-classes removed, replaced by display classes.
- [Typesettings](https://github.com/ianrose/typesettings) in favor of [Plumber](https://github.com/jamonserrano/plumber-sass)

## [4.2.0-4.2.2] - 2018-05-01
### Fixed
- Automated publishing and tagging.

## [4.1.2] - 2018-05-07
### Changed
- The default value of variable $grid-gutter changed to 15px
- Updated styles related to $grid-gutter variable
- Container and container-fluid width value changed to 100%
- Updated formatting for grid styles
- Added "else" to "ignoreAtRules" inside .stylelintrc.js (does not require an empty line before the rule)

## [4.1.1] - 2018-05-01
### Fixed
- Default spinner color.

## [4.1.0] - 2018-04-25
### Added
- Notification close option.

## [4.0.7] - 2018-04-25
### Fixed
- Active menu items.

## [4.0.6] - 2018-04-19
### Fixed
- Animation triggers previously on all, now on specific ones.

### Removed
- `.slide` and `.slide-up` (not used).

## [4.0.5] - 2018-03-30
### Fixed
- Fixed click listener outside notification, this allows for multiple notifications triggers on the same page.

### Added
- Notification initialization can be performed explicitly from code instead of on DOMContentLoaded event.

## [4.0.4] - 2018-03-26
### Fixed
- Expected empty line before at-rule

## [4.0.1-4.0.3] - 2018-03-23
### Added
- Variables added (`$nav-mobile-logo-top`, `$nav-mobile-toggle-top`, `$nav-mobile-logo-mar-bottom` and `$nav-right-top`)

### Fixed
- Small checkbox fix
- Fix nav alignment

## [4.0.0] - 2018-03-23
### Added
- Changelog added
- [Stylelint](https://stylelint.io/) added
- `.list-check` is added to the docs
- `.check-input` and `.check-label` to fix max nesting depth
- `.nav-label` and `.nav-menu-wrap` to fix max nesting depth
- Simple Jekyll Search added as a npm module.
- Extra variables added for easier navigation integration (`$nav-mobile-background`, `$nav-mobile-breakpoint`, `$nav-btn-style`, `$nav-btn-hover-style`, `$nav-height`, `$nav-logo-left`, `$nav-logo-top`, and `$nav-toggle-color`).

### Changed
- Refactored checkbox SCSS to fix max nesting depth
- `.checkbox` renamed to `.check`
- `.checkbox-switch` renamed to `.check-switch`
- `.check-switch` is now placed on the input element
- `.checkbox-wrap` renamed to `.check-wrap`
- Refactored navigation menu to fix max nesting depth
- Styling for `#nav-toggle` moved to `.nav-toggle` (no ID selectors)
- SCSS function `borderPadding` is replaced by `border-padding` (specify kebab-case for function names)
- SCSS function `emScale` is replaced by `em-scale` (specify kebab-case for function names)
- SCSS function `emRhythm` is replaced by `em-rhythm` (specify kebab-case for function names)
- SCSS function `stripUnits` is replaced by `strip-units` (specify kebab-case for function names)
- SCSS function `gridLines` is replaced by `grid-lines` (kebab-case for functions)
- SCSS mixin `setSizeAndLineHeight` is replaced by `set-size-and-line-height` (kebab-case for mixins)
- SCSS mixin `widthPaddingBorderBottom` is replaced by `width-padding-border-bottom` (kebab-case for mixins)
- SCSS mixin `widthPaddingBorderTop` is replaced by `width-padding-border-top` (kebab-case for mixins)

### Fixed
- Fixed ESLint error on Simple Jekyll Search.
- Fixed search results box overflow

### Removed
- `.list-flow` is removed, as it is too project specific.
- Slider with navigation documentation, too project specific.
- `width: 100%` for `.slider-item`s that have a `.bg-image`-class, as it didn't make any changes to the view.
- Simple Jekyll Search minified script removed.
