---
layout: docs
title: Typography
group: base
---

{{ site.projectname }} uses modular scale, vertical rhythm, and responsive ratio based headlines for typography styling. By default it has a `8px` baseline. Furthermore, the following utilities can be used to add additional styles to texts.

### Headings

All HTML headings, `<h1>` through `<h6>`, are available.

{% example html %}
<h1>h1. {{ site.projectname }} heading</h1>
<h2>h2. {{ site.projectname }} heading</h2>
<h3>h3. {{ site.projectname }} heading</h3>
<h4>h4. {{ site.projectname }} heading</h4>
<h5>h5. {{ site.projectname }} heading</h5>
<h6>h6. {{ site.projectname }} heading</h6>
{% endexample %}

`.h1` through `.h6` classes are also available, for when you want to match the font styling of a heading but cannot use the associated HTML element.

{% highlight html %}
<p class="h1">h1. {{ site.projectname }} heading</p>
<p class="h2">h2. {{ site.projectname }} heading</p>
<p class="h3">h3. {{ site.projectname }} heading</p>
<p class="h4">h4. {{ site.projectname }} heading</p>
<p class="h5">h5. {{ site.projectname }} heading</p>
<p class="h6">h6. {{ site.projectname }} heading</p>
{% endhighlight %}

#### Customizing headings

Use the included utility classes to recreate the small secondary heading text from {{ site.projectname }} 3.

{% example html %}
<h3>
  Fancy display heading
  <small class="clr-gray">With faded secondary text</small>
</h3>
{% endexample %}

### Lead

Make a paragraph stand out by adding `.lead`.

{% example html %}
<p class="lead">
  Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
</p>
{% endexample %}

### Font families

{% example html %}
<span>Visualize text inside your element with three different font families:</span>
<ul>
  <li><span class="font-sans">sans serif</span></li>
  <li><span class="font-serif">serif</span></li>
  <li><span class="font-mono">monospace</span></li>
</ul>
{% endexample %}

### Inline text elements

Styling for common inline HTML5 elements.

{% example html %}
<p>You can use the mark tag to <mark>highlight</mark> text.</p>
<p><del>This line of text is meant to be treated as deleted text.</del></p>
<p><s>This line of text is meant to be treated as no longer accurate.</s></p>
<p><ins>This line of text is meant to be treated as an addition to the document.</ins></p>
<p><u>This line of text will render as underlined</u></p>
<p><small>This line of text is meant to be treated as fine print.</small></p>
<p><strong>This line rendered as bold text.</strong></p>
<p><em>This line rendered as italicized text.</em></p>
<p><kbd>Ctrl</kbd> + <kbd>Z</kbd></p>
<p>This is a sentence with sup<sup>this</sup> and sub<sub>that</sub>.</p>
{% endexample %}

`.mark` and `.small` classes are also available to apply the same styles as `<mark>` and `<small>` while avoiding any unwanted semantic implications that the tags would bring.

While not shown above, feel free to use `<b>` and `<i>` in HTML5. `<b>` is meant to highlight words or phrases without conveying additional importance while `<i>` is mostly for voice, technical terms, etc.

### Abbreviations

Stylized implementation of HTML's `<abbr>` element for abbreviations and acronyms to show the expanded version on hover. Abbreviations with a `title` attribute have a light dotted bottom border and a help cursor on hover, providing additional context on hover and to users of assistive technologies.

Add `.initialism` to an abbreviation for a slightly smaller font-size.

{% example html %}
<p><abbr title="attribute">attr</abbr></p>
<p><abbr title="HyperText Markup Language" class="initialism">HTML</abbr></p>
{% endexample %}

### Blockquotes

For quoting blocks of content from another source within your document. Wrap `<blockquote class="blockquote">` around any <abbr title="HyperText Markup Language">HTML</abbr> as the quote.

{% example html %}
<blockquote class="blockquote">
  <p class="m-b-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
</blockquote>
{% endexample %}

#### Naming a source

Add a `<footer class="blockquote-footer">` for identifying the source. Wrap the name of the source work in `<cite>`.

{% example html %}
<blockquote class="blockquote">
  <p class="m-b-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>
{% endexample %}

#### Reverse layout

Add `.blockquote-reverse` for a blockquote with right-aligned content.

{% example html %}
<blockquote class="blockquote blockquote-reverse">
  <p class="m-b-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>
{% endexample %}

### Lists

#### Unstyled

Remove the default `list-style` and left margin on list items (immediate children only). **This only applies to immediate children list items**, meaning you will need to add the class for any nested lists as well.

{% example html %}
<ul class="list-none">
  <li>Lorem ipsum dolor sit amet</li>
  <li>Consectetur adipiscing elit</li>
  <li>Integer molestie lorem at massa</li>
  <li>Facilisis in pretium nisl aliquet</li>
  <li>Nulla volutpat aliquam velit
    <ul>
      <li>Phasellus iaculis neque</li>
      <li>Purus sodales ultricies</li>
      <li>Vestibulum laoreet porttitor sem</li>
      <li>Ac tristique libero volutpat at</li>
    </ul>
  </li>
  <li>Faucibus porta lacus fringilla vel</li>
  <li>Aenean sit amet erat nunc</li>
  <li>Eget porttitor lorem</li>
</ul>
{% endexample %}

#### Inline

Remove a list's bullets and apply some light `margin` with a combination of two classes, `.list-inline`.

{% example html %}
<ul class="list-inline">
  <li class="list-inline-item">Lorem ipsum</li>
  <li class="list-inline-item">Phasellus iaculis</li>
  <li class="list-inline-item">Nulla volutpat</li>
</ul>
{% endexample %}

#### Description list alignment

Align terms and descriptions horizontally by using our grid system's predefined classes (or semantic mixins). For longer terms, you can optionally add a `.text-truncate` class to truncate the text with an ellipsis.

{% example html %}
<dl class="row">
  <dt class="col-sm-3">Description lists</dt>
  <dd class="col-sm-9">A description list is perfect for defining terms.</dd>

  <dt class="col-sm-3">Euismod</dt>
  <dd class="col-sm-9">Vestibulum id ligula porta felis euismod semper eget lacinia odio sem nec elit.</dd>
  <dd class="col-sm-9 col-sm-offset-3">Donec id elit non mi porta gravida at eget metus.</dd>

  <dt class="col-sm-3">Malesuada porta</dt>
  <dd class="col-sm-9">Etiam porta sem malesuada magna mollis euismod.</dd>

  <dt class="col-sm-3 text-truncate">Truncated term is truncated</dt>
  <dd class="col-sm-9">Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</dd>

  <dt class="col-sm-3">Nesting</dt>
  <dd class="col-sm-9">
    <dl class="row">
      <dt class="col-sm-4">Nested definition list</dt>
      <dd class="col-sm-8">Aenean posuere, tortor sed cursus feugiat, nunc augue blandit nunc.</dd>
    </dl>
  </dd>
</dl>
{% endexample %}

#### Checklists

Use checklists, users will love them!

{% example html %}
<ul class="list-check">
  <li>First check</li>
  <li>Second check</li>
  <li>Checkmark!</li>
</ul>
{% endexample %}

Also available in white:

<div class="bg-primary p-3">
  <ul class="list-check list-check-white mb-0">
    <li>First check</li>
    <li>Second check</li>
    <li>Checkmark!</li>
  </ul>
</div>

{% highlight html %}
<ul class="list-check list-check-white">
  <li>First check</li>
  <li>Second check</li>
  <li>Checkmark!</li>
</ul>
{% endhighlight %}{: class="mt-2"}

#### Breadcrumb lists

Use breadcrumbs, they're really nice!

{% example html %}
  <ul class="list-breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Second</a></li>
    <li>Third</li>
  </ul>
{% endexample %}
