---
layout: docs
title: Forms
group: components
---

With {{ site.projectname }} you can easily add every form that you want.

### Textual inputs

Here are examples of each textual HTML5 `<input>` type.

{% example html %}
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-text-input-0">Text</label>
  </div>
  <div class="col-10">
    <input type="text" value="Lorem ipsum" id="example-text-input-0">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-search-input">Search</label>
  </div>
  <div class="col-10">
    <input type="search" value="How do I shoot web" id="example-search-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-email-input">Email</label>
  </div>
  <div class="col-10">
    <input type="email" value="bootstrap@example.com" id="example-email-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-url-input">URL</label>
  </div>
  <div class="col-10">
    <input type="url" value="https://getbootstrap.com" id="example-url-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-tel-input">Telephone</label>
  </div>
  <div class="col-10">
    <input type="tel" value="1-(555)-555-5555" id="example-tel-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-password-input">Password</label>
  </div>
  <div class="col-10">
    <input type="password" value="hunter2" id="example-password-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-number-input">Number</label>
  </div>
  <div class="col-10">
    <input type="number" value="42" id="example-number-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-datetime-local-input">Date and time</label>
  </div>
  <div class="col-10">
    <input class="input" type="datetime-local" value="2011-08-19T13:45:00" id="example-datetime-local-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-date-input">Date</label>
  </div>
  <div class="col-10">
    <input type="date" value="2011-08-19" id="example-date-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-month-input">Month</label>
  </div>
  <div class="col-10">
    <input type="month" value="2011-08" id="example-month-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-week-input">Week</label>
  </div>
  <div class="col-10">
    <input type="week" value="2011-W33" id="example-week-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-time-input">Time</label>
  </div>
  <div class="col-10">
    <input type="time" value="13:45:00" id="example-time-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-color-input">Color</label>
  </div>
  <div class="col-10">
    <input type="color" value="#563d7c" id="example-color-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-file-input">File</label>
  </div>
  <div class="col-10">
    <input type="file" id="example-file-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label>Static</label>
  </div>
  <div class="col-10">
    <span class="input input-static">Some static input</span>
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label>Disabled</label>
  </div>
  <div class="col-10">
    <input type="text" disabled value="Disabled input field" id="example-disabled-input">
  </div>
</div>
<div class="row middle mb-2">
  <div class="col-2">
    <label>Select</label>
  </div>
  <div class="col-10">
    <select>
      <option>Option 1</option>
      <option>Option 2</option>
      <option>Option 3</option>
    </select>
  </div>
</div>
{% endexample %}

#### Textarea

{% example html %}
<div class="row mb-2">
  <div class="col-2">
    <label for="example-textarea-input">Textarea</label>
  </div>
  <div class="col-10">
    <textarea id="example-textarea-input" rows="3"></textarea>
  </div>
</div>
{% endexample %}

#### Inline

{% example html %}
<div class="row middle mb-2">
  <div class="col-2">
    <label for="example-text-input-1">Text</label>
  </div>
  <div class="col-10">
    <input type="text" value="Lorem ipsum" id="example-text-input-1">
  </div>
</div>
{% endexample %}

#### Below

{% example html %}
<div class="row mb-2">
  <div class="col-12">
    <label for="example-text-input-2">Text</label>
    <input type="text" value="Lorem ipsum" id="example-text-input-2">
  </div>
</div>
{% endexample %}

#### Disabled text

{% example html %}
<div class="row mb-2">
  <div class="col-12">
    <label for="example-text-input-3">Text</label>
    <input type="text" value="Lorem ipsum" id="example-text-input-3" value="Lorem ipsum" disabled>
  </div>
</div>
{% endexample %}

### Checkboxes

True or false, let's check this!

#### Default

{% example html %}
<div class="check mb-2">
  <input class="check-input" type="checkbox" id="checkbox-example">
  <label class="check-label" for="checkbox-example"></label>
</div>
{% endexample %}

#### With alternate color

{% example html %}
<div class="check-wrap mb-2">
  <div class="check bg-primary clr-white">
    <input class="check-input" type="checkbox" id="checkbox-example2">
    <label class="check-label" for="checkbox-example2"></label>
  </div>
</div>
{% endexample %}

#### Disabled checked

{% example html %}
<div class="check mb-2">
  <input class="check-input" type="checkbox" id="checkbox-example3" checked disabled>
  <label class="check-label" for="checkbox-example3"></label>
</div>
{% endexample %}

#### Disabled unchecked

{% example html %}
<div class="check mb-2">
  <input class="check-input" type="checkbox" id="checkbox-example4" disabled>
  <label class="check-label" for="checkbox-example4"></label>
</div>
{% endexample %}

#### With label

{% example html %}
<div class="check-wrap mb-2">
  <div class="check">
    <input class="check-input" type="checkbox" id="checkbox-example5">
    <label class="check-label" for="checkbox-example5"></label>
  </div>
  <label for="checkbox-example5">Example label</label>
</div>
{% endexample %}

#### Switch

{% example html %}
<div class="check mb-2">
  <input class="check-input check-switch" type="checkbox" id="checkbox-switch-example">
  <label class="check-label" for="checkbox-switch-example"></label>
</div>
{% endexample %}

And with text label:

{% example html %}
<div class="check-wrap mb-2">
  <div class="check">
    <input class="check-input check-switch" type="checkbox" id="checkbox-switch-example2">
    <label class="check-label" for="checkbox-switch-example2"></label>
  </div>
  <label for="checkbox-switch-example2">Example label</label>
</div>
{% endexample %}

### Validation

Give me the errors!

#### Info

<div class="row mb-2">
  <div class="col-12 feedback info">
    <label for="example-text-input-8">Text</label>
    <input type="text" value="Lorem ipsum" id="example-text-input-8">
    <span class="help clr-primary"><span class="icon clr-primary">{% icon info-with-circle.svg %}</span> This is the info message!</span>
  </div>
</div>

{% highlight html %}
<div class="row mb-2">
  <div class="col-12 feedback info">
    <label for="example-text-input-3">Text</label>
    <input type="text" value="Lorem ipsum" id="example-text-input-3">
    <span class="help clr-primary"><span class="icon clr-primary">{% raw %}{% icon info-with-circle.svg %}{% endraw %}</span> This is the info message!</span>
  </div>
</div>
{% endhighlight %}

<div class="row mar-bottom">
  <div class="col-12 feedback info">
    <label for="test">Textarea</label>
    <textarea id="test" rows="3"></textarea>
    <span class="help clr-primary"><span class="icon clr-primary">{% icon info-with-circle.svg %}</span> This is the info message!</span>
  </div>
</div>

{% highlight html %}
<div class="row mar-bottom">
  <div class="col-12 feedback info">
    <label for="example-textarea-2">Textarea</label>
    <textarea id="example-textare-2" rows="3"></textarea>
    <span class="help clr-primary"><span class="icon clr-primary">{% icon info-with-circle.svg %}</span> This is the info message!</span>
  </div>
</div>
{% endhighlight %}

#### Warning

<div class="row mb-2">
  <div class="col-12 feedback warning">
    <label for="example-text-input-4">Text</label>
    <input type="text" value="Lorem ipsum" id="example-text-input-4">
    <span class="help clr-secondary"><span class="icon clr-secondary">{% icon warning.svg %}</span> This is the warning message!</span>
  </div>
</div>

{% highlight html %}
<div class="row mb-2">
  <div class="col-12 feedback warning">
    <label for="example-text-input-5">Text</label>
    <input type="text" value="Lorem ipsum" id="example-text-input-5">
    <span class="help clr-tertiary"><span class="icon clr-secondary">{% raw %}{% icon warning.svg %}{% endraw %}</span> This is the warning message!</span>
  </div>
</div>
{% endhighlight %}

<div class="row mar-bottom">
  <div class="col-12 feedback warning">
    <label for="test">Textarea</label>
    <textarea id="test" rows="3"></textarea>
    <span class="help clr-secondary"><span class="icon clr-secondary">{% icon warning.svg %}</span> This is the warning message!</span>
  </div>
</div>

{% highlight html %}
<div class="row mar-bottom">
  <div class="col-12 feedback warning">
    <label for="example-textarea-2">Textarea</label>
    <textarea id="example-textare-2" rows="3"></textarea>
    <span class="help clr-secondary"><span class="icon clr-secondary">{% icon warning.svg %}</span> This is the warning message!</span>
  </div>
</div>
{% endhighlight %}

#### Danger
<div class="row mb-2">
  <div class="col-12 feedback danger">
    <label for="example-text-input-6">Text</label>
    <input type="text" value="Lorem ipsum" id="example-text-input-6">
    <span class="help clr-tertiary"><span class="icon clr-tertiary">{% icon new.svg %}</span> This is the danger message!</span>
  </div>
</div>

{% highlight html %}
<div class="row mb-2">
  <div class="col-12 feedback danger">
    <label for="example-text-input-7">Text</label>
    <input type="text" value="Lorem ipsum" id="example-text-input-7">
    <span class="help clr-tertiary"><span class="icon clr-tertiary">{% raw %}{% icon new.svg %}{% endraw %}</span> This is the danger message!</span>
  </div>
</div>
{% endhighlight %}

<div class="row mar-bottom">
  <div class="col-12 feedback danger">
    <label for="test">Textarea</label>
    <textarea id="test" rows="3"></textarea>
    <span class="help clr-tertiary"><span class="icon clr-tertiary">{% icon new.svg %}</span> This is the danger message!</span>
  </div>
</div>

{% highlight html %}
<div class="row mar-bottom">
  <div class="col-12 feedback danger">
    <label for="example-textarea-2">Textarea</label>
    <textarea id="example-textare-2" rows="3"></textarea>
    <span class="help clr-tertiary"><span class="icon clr-tertiary">{% icon new.svg %}</span> This is the danger message!</span>
  </div>
</div>
{% endhighlight %}
