---
layout: docs
title: Slider
group: interaction
---

[Slide to the left, slide to the right! Criss cross, criss cross, cha cha real smooth!](https://www.youtube.com/watch?v=wZv62ShoStY)

### Slider

You can use this flexible slider everywhere. The slider will pause on hover, slider navigation can be placed outside the slider and you should define your own wrap.

{% example html %}
<div>
  <div class="slider-home-wrap">
    <div id="slider-home" class="slider" data-autoplay="true">
      <div class="slider-item active bg-primary" data-slide="0">
        Slide 1
      </div>
      <div class="slider-item bg-secondary" data-slide="1">
        Slide 2
      </div>
      <div class="slider-item bg-tertiary" data-slide="2">
        Slide 3
      </div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-12 align-center">
    <ul class="slider-nav" data-slider-nav="slider-home">
      <li class="bg-primary active" data-slide-go="0"></li>
      <li class="bg-primary" data-slide-go="1"></li>
      <li class="bg-primary" data-slide-go="2"></li>
    </ul>
  </div>
</div>
{% endexample %}
