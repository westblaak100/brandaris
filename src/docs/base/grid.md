---
layout: docs
title: Grid
group: base
---

{{ site.projectname }} includes a powerful mobile-first grid system for building layouts of all shapes and sizes. It's based on a 12 column layout and has multiple tiers.

### How it works

At a high level, here's how the grid system works:

- There are three major components: containers, rows, and columns.
- Containers—`.container` for fixed width or `.container-fluid` for full width—center your site's contents and help align your grid content. You can use it in combination with `.container-full` for a conatiner without side padding.
- Rows are horizontal groups of columns that ensure your columns are lined up properly.
- Content should be placed within columns, and only columns may be immediate children of rows.
- Column classes indicate the number of columns you'd like to use out of the possible 12 per row.
- If you want three equal-width columns, you can easily use `.col-sm`.
- Column `width`s are set in percentages, so they're always fluid and sized relative to their parent element.
- Columns have horizontal `padding` to create the gutters between individual columns.
- There are five grid tiers, one for each responsive breakpoint: extra small, small, medium, large, and extra large.
- Grid tiers are based on minimum widths, meaning they apply to that one tier and all those above it (e.g., `.col-sm-4` applies to small, medium, large, and extra large devices).

### Auto-layout columns
Add any number of auto sizing columns to a row. Let the grid figure it out.

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row">
  <div class="col-xs">
    One of three columns
  </div>
  <div class="col-xs">
    One of three columns
  </div>
  <div class="col-xs">
    One of three columns
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <rect x="57.1" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <rect x="104.2" y="10" class="clr-primary" width="42.8" height="68.3"/>
    </svg>
  </div>
</div>

### Alignment
Add classes to align elements to the start or end of a row as well as the top, bottom, or center of a column.

#### Start

Align all columns to the start of the `.row`. _This is default._

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row start-xs">
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 70">
    <rect x="10" y="10" class="clr-primary" width="42.8" height="36.9"/>
    <rect x="57.1" y="10" class="clr-primary" width="42.8" height="52.4"/>
    </svg>
  </div>
</div>

#### Center

Align all columns to the center of the `.row`.

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row center-xs">
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 70">
    <rect x="33.6" y="10" class="clr-primary" width="42.8" height="36.9"/>
    <rect x="80.7" y="10" class="clr-primary" width="42.8" height="52.4"/>
    </svg>
  </div>
</div>

#### End

Align all columns to the end of the `.row`.

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row end-xs">
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 70">
    <rect x="57.1" y="10" class="clr-primary" width="42.8" height="36.9"/>
    <rect x="104.2" y="10" class="clr-primary" width="42.8" height="52.4"/>
    </svg>
  </div>
</div>

You can also use different alignments for different viewport sizes:

{% highlight html %}
<div class="row center-xs end-sm start-lg">
  <div class="col-xs-6">
    All together now
  </div>
</div>
{% endhighlight %}

#### Top

Align all columns at the top of the `.row`. _This is default._

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row top-xs">
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-primary" width="42.8" height="36.9"/>
      <rect x="57.1" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <rect x="104.2" y="10" class="clr-primary" width="42.8" height="52.4"/>
    </svg>
  </div>
</div>

#### Middle

Align all columns in the middle of the `.row`.

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row middle-xs">
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="26.4" class="clr-primary" width="42.8" height="36.9"/>
      <rect x="57.1" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <rect x="104.2" y="18.7" class="clr-primary" width="42.8" height="52.4"/>
    </svg>
  </div>
</div>

#### Bottom

Align all columns to the bottom of the `.row`.

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row bottom-xs">
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="41.3" class="clr-primary" width="42.8" height="36.9"/>
      <rect x="57.1" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <rect x="104.2" y="25.8" class="clr-primary" width="42.8" height="52.4"/>
    </svg>
  </div>
</div>


#### Full

Stretch all columns for equal, full, height in the `.row`.

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row full-xs">
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
  <div class="col-4">
    ...
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <rect x="57.1" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <rect x="104.2" y="10" class="clr-primary" width="42.8" height="68.3"/>
    </svg>
  </div>
</div>

### Reordering
Add classes to reorder columns.

#### First

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row">
  <div class="col-xs">
    1
  </div>
  <div class="col-xs">
    2
  </div>
  <div class="col-xs first-xs">
    3
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-primary" width="42.8" height="36.9"/>
      <rect x="57.1" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <rect x="104.2" y="10" class="clr-primary" width="42.8" height="52.4"/>
      <text transform="matrix(1 0 0 1 26.5 32.616)" class="clr-white h6">3</text>
      <text transform="matrix(1 0 0 1 74.4 32.616)" class="clr-white h6">1</text>
      <text transform="matrix(1 0 0 1 121.2 32.616)" class="clr-white h6">2</text>
    </svg>
  </div>
</div>

#### Last

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row">
  <div class="col-xs last-xs">
    1
  </div>
  <div class="col-xs">
    2
  </div>
  <div class="col-xs">
    3
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-primary" width="42.8" height="52.4"/>
      <rect x="57.1" y="10" class="clr-primary" width="42.8" height="36.9"/>
      <rect x="104.2" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <text transform="matrix(1 0 0 1 26.5 32.616)" class="clr-white h6">2</text>
      <text transform="matrix(1 0 0 1 74.4 32.616)" class="clr-white h6">3</text>
      <text transform="matrix(1 0 0 1 121.2 32.616)" class="clr-white h6">1</text>
    </svg>
  </div>
</div>

#### Reverse

<div class="row">
  <div class="col-12 col-lg-6">

{% highlight html %}
<div class="row reverse">
  <div class="col-xs">
    1
  </div>
  <div class="col-xs">
    2
  </div>
  <div class="col-xs">
    3
  </div>
</div>{% endhighlight %}

  </div>
  <div class="col-12 col-lg-6">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 88.3">
      <rect x="10" y="10" class="clr-primary" width="42.8" height="36.9"/>
      <rect x="57.1" y="10" class="clr-primary" width="42.8" height="52.4"/>
      <rect x="104.2" y="10" class="clr-primary" width="42.8" height="68.3"/>
      <text transform="matrix(1 0 0 1 26.5 32.616)" class="clr-white h6">3</text>
      <text transform="matrix(1 0 0 1 74.4 32.616)" class="clr-white h6">2</text>
      <text transform="matrix(1 0 0 1 121.2 32.616)" class="clr-white h6">1</text>
    </svg>
  </div>
</div>
