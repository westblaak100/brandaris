---
layout: docs
title: Toggle
group: interaction
---

Do you want it all or do you want to play hide and seek? Mr. Toggle is there for you!

### Toggle all

You can toggle all checkboxes with another one by adding the class `dataToggleAll` and set `data-toggle-all` and `data-target`. Each chechbox that should be toggled should have the same classname as given in `data-target`.

{% example html %}
<div class="row middle mb-2">
  <div class="col-10">
    Checkbox 1
  </div>
  <div class="col-2">
    <input type="checkbox" class="toggleAll">
  </div>
  <div class="col-10">
    Checkbox 2
  </div>
  <div class="col-2">
    <input type="checkbox" class="toggleAll">
  </div>
  <div class="col-10">
    Checkbox 3
  </div>
  <div class="col-2">
    <input type="checkbox" class="toggleAll">
  </div>
  <div class="col-10">
    Toggle all
  </div>
  <div class="col-2">
    <input type="checkbox" class="dataToggleAll" data-toggle-all="true" data-target="toggleAll">
  </div>
</div>
{% endexample %}

### Toggle show

Welcome to the great magic show! The best show in town where will magically hide and show content.

Just use the class `dataToggle` and set `data-toggle` in combination with the target ID placed in `data-target` and give some alternative text if you like with `data-alttext`.

{% example html %}
<div class="box bg-gray-light mb-2">
  <div class="box-header bg-secondary">
    <h4>Header</h4>
  </div>
  <div id="showHide" class="pt-2">
    This is magic!
  </div>
  <div class="box-footer">
    <button type="button" class="btn btn-secondary dataToggle" data-toggle="true" data-target="showHide" data-alttext="Show it!">Hide it!</button>
  </div>
</div>
{% endexample %}
