![Brandaris CSS-only framework](https://bitbucket.org/maartenbrakkee/brandaris/raw/default/src/assets/images/repository.jpg)

## System Preparation

### Required tools

_When updating from <3.0.0, see section below._

To use the front end develop environment for the projects, you'll need the following things installed on your machine.

1. [NodeJS](http://nodejs.org) - use the installer.
2. [Ruby](https://www.ruby-lang.org/) - use an installer like [RubyInstaller](https://rubyinstaller.org/).
3. [Jekyll](http://jekyllrb.com/) - `$ gem install jekyll:3.7.2 jekyll-sitemap:1.2.0 kramdown:1.16.2 rouge:3.1.1` (use `gem cleanup` for removing older versions)

#### Updating from < 3.0.0

Start clean, remove old dependencies before installing the required tools.

##### Update npm

_Run as administrator._

```shell
$ npm install -g npm
```

##### Remove old modules:

```shell
$ rm -rf node_modules
$ gem cleanup jekyll jekyll-sitemap kramdown rouge
```

### Recommended tools

- [Atom](https://atom.io/)

    Atom is a fast, goodlooking and extendable text editor by GitHub.

- [Cmdr](http://cmder.net/)

    Console emulator for Windows, based on [Conemu](https://conemu.github.io/). For context menu integration open terminal in installation dir (preferably `C:\cmder`) as Administrator and run `.\cmder.exe /REGISTER ALL`.

    _Mini ([download](https://github.com/cmderdev/cmder/releases/latest)) version should be enough._

## Local Installation

After installing required tools, run the following command inside the `brandaris` directory:

```shell
$ npm install
```

## Usage

For running development enviroment with Hot Module Replacement (HMR) and [Browsersync](https://browsersync.io/), run the following command:

```shell
$ npm run start
```

### Building production

The production build contains minified CSS and JavaScript. For building production run:

```shell
$ npm run build
```

### Configuration files

By default there are development/production settings for Jekyll and Webpack.

| File   | Development | Production | Note |
|--------|:-----------:|:----------:|------|
| browser-sync.js |  ✅ | ❌ | |
| jekyll.dev.yml | ✅ | ❌ | Jekyll development specific settings, overrules production settings. |
| jekyll.yml | ✅ | ✅ | Jekyll default settings. |
| webpack.common.js | ✅ | ✅ | Webpack settings used by development and production. |
| webpack.dev.js | ✅ | ❌ | |
| webpack.prod.js | ❌ | ✅ | | |

## License

Copyright (c) 2016 Maarten Brakkee

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
