"use strict";

import { forEach } from "JS/functions";

export function initTabs() {
  // Grab all .tabs and .tabs-navs
  var tabNavs = document.querySelectorAll(".tabs-nav");

  // Loop all .tabs-navs
  forEach(tabNavs, function (tabNav) {
    var tabID = tabNav.getAttribute("data-tabs-nav");

    // Listen to click event and change tab
    forEach(tabNav.children, function (child, j) {
      child.addEventListener("click", function() {
        var currentTab = childWithClassIndex(tabNav,"active");
        tabSwitch(tabID, currentTab, j);
      });
    });
  });
  // Change tabs!
  function tabSwitch(tabID, currentTab, goToTab) {
    var tabs = document.querySelectorAll("#"+ tabID)[0];

    if(goToTab === undefined) {
      if((currentTab + 1) < tabs.children.length) {
        goToTab = currentTab + 1;
      } else {
        goToTab = 0;
      }
    }

    // Only change tabs when goToTab is different
    if(currentTab !== goToTab) {
      var tabNav = document.querySelectorAll("*[data-tabs-nav='"+ tabID +"']")[0],
        newActiveTab = document.querySelectorAll("#"+ tabID + " div[data-tab='"+ goToTab +"']")[0];

      // Remove .active class from all navigation items
      removeChildClass(tabNav, "active");
      // Add .active class to navigation item
      tabNav.querySelectorAll("[data-tab-go='"+ goToTab +"']")[0].classList.add("active");

      // Remove .active tab class from all tabs
      removeChildClass(tabs, "active");
      // Add .active class
      newActiveTab.classList.add("active");
    }
  }

  // Remove specific class from all element's children
  function removeChildClass(element, className) {
    forEach(element.children, function (el) {
      el.classList.remove(className);
    });
  }

  // Return the index for an element's child with a specific class
  function childWithClassIndex(element, className) {
    var childClass = false;
    forEach(element.children, function (el, i) {
      if(el.classList.contains(className)) {
        childClass = i;
      }
    });
    return childClass;
  }
}

document.addEventListener("DOMContentLoaded", initTabs);
