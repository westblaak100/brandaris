"use strict";

import { forEach } from "JS/functions";

export function initNotifications(htmlDocument) {
  var triggers = htmlDocument.querySelectorAll("[data-notification]");

  forEach(triggers, function (trigger) {
    var id = trigger.dataset.notification,
      notification = htmlDocument.querySelectorAll("#"+id)[0],
      html = document.querySelectorAll("html")[0],
      shown = false;

    // clicked outside of notification
    function htmlClicked(e) {
      if(shown && ( (e.target !== trigger && Array.from(trigger.getElementsByTagName("*")).indexOf(e.target)===-1) && notification.dataset.dismissable === "true")) {
        showHideNotification(notification);
        shown = !shown;
      }
    }

    trigger.onclick = function() {
      showHideNotification(notification);
      shown = !shown;
    };

    trigger.removeClickListener = function () {
      html.removeEventListener("click", htmlClicked);
    };

    // click outside notification
    html.addEventListener("click", htmlClicked);

    // trigger dismiss
    var dismiss = notification.querySelectorAll("[data-dismiss]");
    forEach(dismiss, function (elm) {
      elm.addEventListener("click", htmlClicked);
    });

    notification.showHide = function () {
      showHideNotification(notification);
      shown = !shown;
    };

    // click inside notification
    notification.onclick = function(e){
      e.stopPropagation();
    };
  });
}

function showHideNotification(notification, html = document.querySelectorAll("html")[0]) {
  notification.classList.toggle("open");
  html.classList.toggle("notification-open");
}

export function removeOnClickListeners(htmlDocument) {
  var triggers = htmlDocument.querySelectorAll("[data-notification]");
  forEach(triggers, function (trigger) {
    trigger.removeClickListener();
  });
}

document.addEventListener("DOMContentLoaded", function () {
  initNotifications(document);
});
